package Server

import (
	"context"
	"fmt"
	//"github.com/davecgh/go-spew/spew"
  	"html/template"
	. "github.com/logrusorgru/aurora"
	"github.com/rs/cors"
	Home "gitlab.com/zendrulat123/go/Handlers/Home"
	
	"io"
	"log"
	"net/http"
	"net/url"
	"time"
)
type ViewData struct {
  Name string
}
var err error

type Contexter struct {
	M string
	U *url.URL
	P string
	B    io.ReadCloser
	//Gb   func() (io.ReadCloser, error)
	Host string
	Form url.Values
	Cancel <-chan struct{}
	R      *http.Response
	//H      http.Header
  D Duration
}
 var CC Contexter
  

func AddContext(ctx context.Context, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
     Start := time.Now()
		Duration := time.Now().Sub(Start)
    
    spew.Dump(Duration)
		CC = Contexter{
			M:      r.WithContext(ctx).Method,
			U:      r.WithContext(ctx).URL,
			P:      r.WithContext(ctx).Proto,
			B:      r.WithContext(ctx).Body,
			Host:   r.WithContext(ctx).Host,
			Form:   r.WithContext(ctx).Form,
			Cancel: r.WithContext(ctx).Cancel,
			R:      r.WithContext(ctx).Response,
      D: Duration
			//H:      r.WithContext(ctx).Header,
		}
    
    

	
		fmt.Println(Blue("/ʕ◔ϖ◔ʔ/````````````````````````````````````````````"))
		fmt.Printf("Method:%s\n - URL:%s\n - Proto:%s\n - Body:%v\n - Host:%s\n - Form:%v\n - Cancel:%d\n - Response:%d\n - Dur:%02d-00:00 \n - DBConn:%v \r\n  - Header:%s\n ",
			Cyan(CC.M),
			Red(CC.U),
			Brown(CC.P),
			Blue(CC.B),
			Yellow(CC.Host),
			BgRed(CC.Form),
			BgGreen(CC.Cancel),
			BgBrown(CC.R),
			BgMagenta(CC.D),
			Red(DBs.DBS().Stats()),
			Green(CC.H),
		)

		cookie, _ := r.Cookie("username")

		if cookie != nil {
			//Add data to context
			ctx := context.WithValue(r.Context(), "Username", cookie.Value)
			next.ServeHTTP(w, r.WithContext(ctx))

		} else {

			if err != nil {
				// Error occurred while parsing request body
				w.WriteHeader(http.StatusBadRequest)

				return
			}
			next.ServeHTTP(w, r.WithContext(ctx))
		}
	})
}

func StatusPage(w http.ResponseWriter, r *http.Request) {
	//Get data from context
	if username := r.Context().Value("Username"); username != nil {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Hello " + username.(string) + "\n"))
	} else {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Not Logged in"))
	}
}

//starts the server
func Serv() {

	mux := http.NewServeMux()
	//handlers

	mux.HandleFunc("/search", Search.Search)
	mux.HandleFunc("/", StatusPage)
	mux.HandleFunc("/home", Home.Home)
	mux.HandleFunc("/sayhi", Login.SayhelloName)
	mux.HandleFunc("/login", Login.Login)
	mux.HandleFunc("/thankyou", Thankyou.Thankyou)
	mux.HandleFunc("/fuser", Search.FUser)
	mux.HandleFunc("/logger", Logger)
	handler := cors.Default().Handler(mux)
	c := context.Background()
	log.Fatal(http.ListenAndServe(":8082", AddContext(c, handler)))

}

var tpl *template.Template


func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}


func Logger(w http.ResponseWriter, r *http.Request) {


	switch r.Method {
	case "GET":
		err := tpl.ExecuteTemplate(w, "logger.html", CC)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}

	case "POST":
		fmt.Println(r.Header.Get("Origin"))
		err := tpl.ExecuteTemplate(w, "logger.html", CC)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}

	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")
	}

}




